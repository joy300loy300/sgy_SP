#include <stdio.h>

int main() {
    unsigned char arr[] = { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 };
    int sum = 0;
    
    for (int i = 0; i < 10; i++) {
        if (i % 2 == 0) {
            arr[i] <<= 1;
        } else {
            arr[i] >>= 1;
        }
        sum += arr[i];
    }
    
    printf("Сумма элементов полученного массива: %d\n", sum);
    
    return 0;
}
