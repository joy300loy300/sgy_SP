#!/bin/bash
echo "Семенов Григорий"
echo "Менеджер файлов Lite"
echo "Программа позволяет создавать, удалять и перемещать файлы"
while true
do
    echo "Выберите тип операции над файлом: создать - 1, удалить - 2, переместить - 3"
    read -p "" MyOper
    if [ "$MyOper" == "1" ]; then
        echo "Введите имя нового файла:"
        read filename   
        if [ -e "$filename" ]; then
            echo "Ошибка: файл уже существует"
        else
            touch "$filename"
            echo "Файл успешно создан"
        fi
    elif [ "$MyOper" == "2" ]; then
        echo "Введите имя файла, который нужно удалить:"
        read filename
    
        if [ ! -e "$filename" ]; then
            echo "Ошибка: файл не существует"
        else
            rm "$filename"
            echo "Файл успешно удален"
        fi
    elif [ "$MyOper" == "3" ]; then
        echo "Введите имя файла, который нужно переместить:"
        read filename   
        if [ ! -e "$filename" ]; then
        echo "Ошибка: файл не существует"
        else
            echo "Введите путь к каталогу, в который нужно переместить файл:"
            read directory
            if [ ! -d "$directory" ]; then
                echo "Ошибка: каталог не существует"
            else
                mv "$filename" "$directory"
                echo "Файл успешно перемещен"
            fi
        fi
    else
        echo "Ошибка: неверный тип операции"
    fi
    while true
    do
        echo "Выберите выберите действие: продолжить работу с менеджером - 1, выйти - 2"
        read NextAction
        if [ "$NextAction" == "1" ]; then
            break
        fi
        if [ "$NextAction" == "2" ]; then
            break
        fi
    done
    if [ "$NextAction" == "2" ]; then
        break
    fi
done       
