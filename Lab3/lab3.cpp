#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main() {
    srand(time(NULL)); // Инициализируем генератор случайных чисел

    int size = rand() % 10 + 10; // Сгенерируем случайное число для размера массива

    int* arr = new int[size]; // Создаем динамический массив заданного размера
    int med = size / 2 + size % 2;

    // Заполняем массив симметричными значениями
    asm(
        "movl $0, %%eax\n\t"     // Устанавливаем начальное значение eax в 0
        "movl %0, %%ebx\n\t"     // Загружаем размер массива в ebx
        "movl %1, %%edi\n\t"     // Загружаем адрес начала массива в edi
        "movl %2, %%ecx\n\t"     // Загружаем значение med в ecx
        "1:\n\t"
        "movl %%eax, (%1,%%eax,4)\n\t"   // Записываем значение eax в [arr+eax*4]
        "movl %%eax, (%1,%%ebx,4)\n\t"   // Записываем значение eax в [arr+ebx*4]
        "incl %%eax\n\t"         // Увеличиваем eax на 1
        "decl %%ebx\n\t"         // Уменьшаем ebx на 1
        "cmp %%eax, %%ecx\n\t"   // Сравниваем eax и ecx
        "jle 2f\n\t"             // Если eax <= ecx, переходим к метке 2
        "jmp 1b\n\t"             // Возвращаемся к метке 1
        "2:\n\t"
        :
        : "g" (size), "g" (arr), "g" (med)
        : "eax", "ebx", "ecx", "edi"
    );

    // Выводим массив на экран
    for (int i = 0; i < size; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;

    // Освобождаем выделенную память
    delete[] arr;

    return 0;
}

