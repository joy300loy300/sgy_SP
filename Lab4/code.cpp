#include "mbed.h"
// Объявляем пин светодиода 1 как выход
DigitalOut oled1(LED1);
int c = 0;
InterruptIn ibutton1(BUTTON1);
static auto sleep_time = 0ms;
Ticker toggle_led_ticker;
void led_ticker()
{
  oled1=!oled1;
}
// Основной поток main
void pressed()
{
  c++;
 
  if ( c == 1 )
  {
    oled1=1;
    sleep_time = 1000000ms;
    toggle_led_ticker.attach(&led_ticker, sleep_time);
  }
  if ( c == 2 )
  {
    sleep_time = 1000ms;
    toggle_led_ticker.attach(&led_ticker, sleep_time);
  //oled1 = 1; 
  }
  if ( c == 3)
  {
    sleep_time = 250ms;
    toggle_led_ticker.attach(&led_ticker, sleep_time);
  //oled1 = 1; 
  c = 0;
  }
}




int main()
{
  oled1 = 0;
  ibutton1.rise(&pressed);
  while (true) {}
}
