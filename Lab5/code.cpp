#include "mbed.h"

DigitalIn button(PC_13);// Объявление глобальных переменных и объектов
Thread defaultThread;
Thread buttonThread;
Mutex mutex;
void defaultTask()// Задача потока по умолчанию
{
    while (true)
    {
        mutex.lock();// Захват мьютекса для защиты доступа к выводу
        printf("Нажмите кнопку...\n");// Вывод информации по умолчанию
        mutex.unlock();// Освобождение мьютекса
        ThisThread::sleep_for(1000);// Задержка на 1 секунду
    }
}
void buttonTask()// Задача потока кнопки
{
    while (true)
    {
        mutex.lock();// Захват мьютекса для защиты доступа к выводу
        printf("Кнопка не нажата.\n");// Вывод информации о кнопке
        mutex.unlock();// Освобождение мьютекса
        ThisThread::sleep_for(1000);// Задержка на 1 секунду
    }
}
int main()
{
    defaultThread.start(defaultTask);// Запуск потоков
    buttonThread.start(buttonTask);
    while (true)// Основной цикл программы
    {
        if (button.read() == 0)// Проверка состояния кнопки
        {
            mutex.lock();// Захват мьютекса для защиты доступа к выводу
            printf("Нажата кнопка!\n");// Вывод информации о нажатой кнопке
            mutex.unlock(); // Освобождение мьютекса
        }
        ThisThread::sleep_for(100); // Задержка на 100 миллисекунд
    }
}
